import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { nanoid } from 'nanoid';
import { BlogPost } from '../../home/blog_interface';
import { Router } from '@angular/router';
import { BlogActionsService } from 'src/app/services/blog-actions.service';

@Component({
  selector: 'app-blog-template',
  templateUrl: './blog-template.component.html',
  styleUrls: ['./blog-template.component.scss']
})
export class BlogTemplateComponent implements OnInit {

  @Input() blog:BlogPost = {
    id: '',
    creator: '',
    title: '',
    description: '',
    isPrivate: false,
    content: ''
  }
  @Input() homeSidebarSelectedOption: string = "public"
  
  deleteBlog(){
    const action:boolean = this.blogService.deleteBlog(this.blog.id);
    if(action===false){
      alert("Cannot Delete");
    }
  }
  editBlog(){
    this.router.navigate(['/home/create-blog',this.blog.id]);
  }

  viewBlog(){
    this.router.navigate(['home/view-blog',this.blog.id]);
  }
  constructor(private router: Router, private blogService: BlogActionsService) { }

  ngOnInit(): void {
    // console.log(this.blog);
  }

}
