import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BlogPost } from '../home/blog_interface';
import { BlogActionsService } from 'src/app/services/blog-actions.service';
import { nanoid } from 'nanoid';

@Component({
  selector: 'app-display-blog',
  templateUrl: './display-blog.component.html',
  styleUrls: ['./display-blog.component.scss']
})
export class DisplayBlogComponent implements OnInit {
  blogId:string ="";
  
  constructor(
    private route: ActivatedRoute,
    private blogService: BlogActionsService
  ) { }

  blog:BlogPost = {
    id: this.blogId,
    creator: '',
    title: '',
    description: '',
    isPrivate: false,
    content: ''
  }
  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('blogId');
    this.blogId = id !== null ? id : '';
    const blogFromService = this.blogService.getBlogFromId(this.blogId);
    if(blogFromService){
      this.blog = blogFromService;
    }
    else{
      this.blog.id = nanoid();
    }
  }

}
