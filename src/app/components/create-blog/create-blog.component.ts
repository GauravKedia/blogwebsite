import { Component, OnInit } from '@angular/core';
import { nanoid } from 'nanoid';
import { BlogPost } from '../home/blog_interface';
import { ActivatedRoute } from '@angular/router';
import { BlogActionsService } from 'src/app/services/blog-actions.service';

@Component({
  selector: 'app-create-blog',
  templateUrl: './create-blog.component.html',
  styleUrls: ['./create-blog.component.scss']
})
export class CreateBlogComponent implements OnInit {
  blogId:string ="";
  
  constructor(
    private route: ActivatedRoute,
    private blogService: BlogActionsService
  ) { }

  blog:BlogPost = {
    id: this.blogId,
    creator: '',
    title: '',
    description: '',
    isPrivate: false,
    content: ''
  }

  isEditMode: boolean = false;

  toggleBlogType() {
    this.blog.isPrivate = !this.blog.isPrivate;
  }

  submitBlog() {
    if(this.blogService.validateBlog(this.blog)){
      if(this.isEditMode){
        const action:boolean = this.blogService.editBlog(this.blog);
        if(action===false){
          alert("Cannot Edit");
        }
      }
      else{
        this.blogService.addBlog(this.blog);
      }
    }
    else{
      alert("Data of Blog Cannot Be Empty");
    }
  }


  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('blogId');
    this.blogId = id !== null ? id : '';
    const blogFromService = this.blogService.getBlogFromId(this.blogId);
    const creatorFromService = this.blogService.getLoggedInUsername();
    if(creatorFromService){
      this.blog.creator = creatorFromService;
    }
    else{
      this.blogService.signOut();
    
    }
    if(blogFromService){
      this.blog = blogFromService;
      this.isEditMode = true;
    }
    else{
      this.blog.id = nanoid();
      this.isEditMode = false;
    }
  }

}
