// import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
// import { BlogPost } from '../blog_interface';
// import { ActivatedRoute, Router } from '@angular/router';
// import { nanoid } from 'nanoid';
// @Component({
//   selector: 'app-create-blog',
//   templateUrl: './create-blog.component.html',
//   styleUrls: ['./create-blog.component.scss']
// })
// export class CreateBlogComponent implements OnInit {
//   @Input() isEditMode: boolean = false;
  
//   @Input() createBlogData: BlogPost = {
//     id: nanoid(),
//     creator: '',
//     title: '',
//     description: '',
//     content: '',
//     isPrivate: false,
//   }

//   @Output() submitCreateBlog = new EventEmitter<BlogPost>();
  
//   submitBlog() {
//     // if (!this.isEditMode) {

//     //   const blogPost: BlogPost = {
//     //     id: this.blogId,
//     //     creator: this.blogUser,
//     //     title: this.blogTitle,
//     //     description: this.blogDescription,
//     //     content: this.blogContent,
//     //     isPrivate: this.blogPrivate,
//     //   };
//     //   const storedBlogPosts: BlogPost[] = JSON.parse(localStorage.getItem('blogPosts') || '[]');
//     //   // console.log(storedBlogPosts);
//     //   storedBlogPosts.push(blogPost);
//     //   localStorage.setItem('blogPosts', JSON.stringify(storedBlogPosts));
      
//     // }
//     // else {
//     //   const storedBlogPosts: BlogPost[] = JSON.parse(localStorage.getItem('blogPosts') || '[]');
//     //   const index = storedBlogPosts.findIndex(blog => blog.title === this.createBlogData.title);

//     //   if (index !== -1) {
//     //     storedBlogPosts[index].creator = this.blogUser;
//     //     storedBlogPosts[index].title = this.blogTitle;
//     //     storedBlogPosts[index].description = this.blogDescription;
//     //     storedBlogPosts[index].content = this.blogContent;
//     //     storedBlogPosts[index].isPrivate = this.blogPrivate;
//     //     localStorage.setItem('blogPosts', JSON.stringify(storedBlogPosts));
//     //   }

//     // }
//     this.submitCreateBlog.emit();

//   }

//   toggleBlogType(){
//     this.createBlogData.isPrivate = !this.createBlogData.isPrivate;
//   }

//   constructor(private route: ActivatedRoute, private router: Router) { }

//   ngOnInit(): void {
    
//     const loggedInUser = localStorage.getItem('loggedInUser');
//     if (loggedInUser) {
//       this.createBlogData.creator = loggedInUser;
//     }
//     else {
//       alert("Please Login First");
//       this.router.navigate(['/login']);
//     }
//     // console.log('createBlogData:', this.createBlogData);
//     // this.blogUser = this.createBlogData.creator;
//     // this.blogTitle = this.createBlogData.title;
//     // this.blogDescription = this.createBlogData.description;
//     // this.blogContent = this.createBlogData.content;
//     // this.blogPrivate = this.createBlogData.isPrivate;
    
//   }

// }