import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BlogPost } from '../blog_interface';
import { nanoid } from 'nanoid';
import { Router } from '@angular/router';
@Component({
  selector: 'app-home-blog',
  templateUrl: './home-blog.component.html',
  styleUrls: ['./home-blog.component.scss']
})
export class HomeBlogComponent implements OnInit {
  @Input() blog:BlogPost = {
    id: nanoid(),
    creator: '',
    title: '',
    description: '',
    isPrivate: false,
    content: ''
  }
  @Input() homeSidebarSelectedOption:string = "blogs";
  
  @Output() deleteBlogClick = new EventEmitter<string>();
  @Output() editBlogClick = new EventEmitter<string>();
  isAdmin: boolean = false;
  
  deleteBlog(){
    this.deleteBlogClick.emit(this.blog.id);
  }
  editBlog(){
    this.editBlogClick.emit(this.blog.id);
  }

  viewBlog(){
    this.router.navigate(['home/viewBlog',this.blog.id]);
  }

  constructor(private router: Router) { }


  ngOnInit(): void {
    const isAdminString = localStorage.getItem('isAdmin');
    this.isAdmin = isAdminString === 'true';
  }

  

}
