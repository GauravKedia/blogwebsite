import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reduceContent'
})
export class ReduceContentPipe implements PipeTransform {

  transform(value: string): string {
    if (!value) return value;
    const words = value.split(' ');
    const reducedContent = words.slice(0, 11).join(' ');
    return reducedContent;
  }

}
