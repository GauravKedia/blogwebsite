import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appHover]'
})
export class HoverDirective {
  elem: ElementRef
  constructor(elem: ElementRef) { this.elem = elem; }
  @HostListener("mouseenter")
  addHighlight() {
    this.elem.nativeElement.style.backgroundColor = "rgb(201 199 199 / 47%)";
  }

  @HostListener("mouseleave")
  removeHighlight() {
    this.elem.nativeElement.style.backgroundColor = "white";
  }
}
