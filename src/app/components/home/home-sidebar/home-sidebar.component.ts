import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { nanoid } from 'nanoid';
import { BlogActionsService } from 'src/app/services/blog-actions.service';

@Component({
  selector: 'app-home-sidebar',
  templateUrl: './home-sidebar.component.html',
  styleUrls: ['./home-sidebar.component.scss']
})
export class HomeSidebarComponent implements OnInit {
  homeSidebarSelectedOption: string = "blogs";
  isAdmin:boolean = false;
  
  createBlog(){
    this.homeSidebarSelectedOption = "createBlog";
    const idForNewBlog = nanoid();
    this.router.navigate(['/home/create-blog',idForNewBlog]);
  }
  loadMyBlog(){
    this.homeSidebarSelectedOption = "myBlogs";
    this.router.navigate(['/home/my-blogs']);
  }
  loadCommonBlog(){
    this.homeSidebarSelectedOption = "blogs";
    this.router.navigate(['/home']);
  }
  ManageUsers(){
    this.router.navigate(['manage']);
  }
  signOutUser(){  
    this.blogService.signOut();
  }
  constructor(private router:Router,private blogService: BlogActionsService) { }

  ngOnInit(): void {
    this.isAdmin = this.blogService.isAdminUser();
  }

}
