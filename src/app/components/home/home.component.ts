import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { nanoid } from 'nanoid';
import { BlogPost } from './blog_interface';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor() { }
  ngOnInit(): void {
  }
}

