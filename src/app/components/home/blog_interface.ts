export interface BlogPost {
    id: string;
    creator: string;
    title: string;
    description:string;
    content: string;
    isPrivate: boolean;
  }