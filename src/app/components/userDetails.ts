export interface UserDetails {
    username: string;
    age: number; // Will calculate this on basis of dateOfBirth
    gender: string;
    password: string;
    profilePhoto: string;
    userType: string;
    dateOfBirth: Date;
    contact: number; // Phone Number of 10 digit only
    countryCode: number; // Store only number add + to display
    height: number; // Storing In inches
  }