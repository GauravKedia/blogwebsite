import { Component, OnInit } from '@angular/core';
import { BlogPost } from '../home/blog_interface';
import { Router } from '@angular/router';
import { BlogActionsService } from 'src/app/services/blog-actions.service';

@Component({
  selector: 'app-public-blogs',
  templateUrl: './public-blogs.component.html',
  styleUrls: ['./public-blogs.component.scss']
})
export class PublicBlogsComponent implements OnInit {

  storedBlogPosts: BlogPost[] = [];
  isEmpty: boolean = false;

  constructor(private router: Router, private blogService: BlogActionsService) { }

  ngOnInit(): void {
    if (this.blogService.validateForPageAccess()) {
      const blogsFromService = this.blogService.loadPublicBlogs();
      if (blogsFromService.length > 0) {
        this.storedBlogPosts = blogsFromService;
      }
      else {
        this.isEmpty = true;
      }
    }
    else{
      alert("Unauthorized Access! Please Login Again");
      this.blogService.signOut();
    }
  }

}
