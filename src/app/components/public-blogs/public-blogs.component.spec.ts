import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicBlogsComponent } from './public-blogs.component';

describe('PublicBlogsComponent', () => {
  let component: PublicBlogsComponent;
  let fixture: ComponentFixture<PublicBlogsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PublicBlogsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PublicBlogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
