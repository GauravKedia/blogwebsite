import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivateBlogsComponent } from './private-blogs.component';

describe('PrivateBlogsComponent', () => {
  let component: PrivateBlogsComponent;
  let fixture: ComponentFixture<PrivateBlogsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrivateBlogsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PrivateBlogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
