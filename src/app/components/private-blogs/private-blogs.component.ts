import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BlogActionsService } from 'src/app/services/blog-actions.service';
import { BlogPost } from '../home/blog_interface';

@Component({
  selector: 'app-private-blogs',
  templateUrl: './private-blogs.component.html',
  styleUrls: ['./private-blogs.component.scss']
})
export class PrivateBlogsComponent implements OnInit {

  storedBlogPosts: BlogPost[] = [];
  isEmpty:boolean = false;
  
  constructor(private router: Router,private blogService: BlogActionsService) {}

  ngOnInit(): void {
    const blogsFromService = this.blogService.loadPrivateBlogs();
    if(blogsFromService.length>0){
      this.storedBlogPosts = blogsFromService;
    }
    else{
      this.isEmpty = true;
    }
  }
}
