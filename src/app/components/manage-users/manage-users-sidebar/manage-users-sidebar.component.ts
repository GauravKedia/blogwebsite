import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { UserDetails } from '../../userDetails';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-manage-users-sidebar',
  templateUrl: './manage-users-sidebar.component.html',
  styleUrls: ['./manage-users-sidebar.component.scss']
})
export class ManageUsersSidebarComponent implements OnInit {
  @Input() users: UserDetails[] = [];
  @Input() selectedUsername:string = "";
  @Output() userSelectedClicked = new EventEmitter<string>();

  searchInput: string = "";
  
  userGotSelected(username:string){
    this.userSelectedClicked.emit(username);
  }

  goToBlogs(){
    this.router.navigate(['/home'])
  }
  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
  }
}
