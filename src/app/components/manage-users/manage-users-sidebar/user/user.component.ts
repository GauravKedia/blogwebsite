import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserDetails } from 'src/app/components/userDetails';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  @Input() user:UserDetails={ username: "",
  age: 0,
  gender: "",
  password: "",
  profilePhoto: "assets/user_images/default_profile.png",
  userType: "User",
  dateOfBirth: new Date(),
  contact: 0,
  countryCode: 0,
  height: 0 
};
  // @Input() isSelected:Boolean;
  @Output() userSelectedClicked = new EventEmitter<string>();
  @Input() selectedUsername:string="";
  userGotSelected(username:string){
    this.userSelectedClicked.emit(username);
  }
  constructor() { }

  ngOnInit(): void {
  }


}
