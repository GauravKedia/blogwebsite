import { Pipe, PipeTransform } from '@angular/core';
import { UserDetails } from '../../userDetails';

@Pipe({
  name: 'sidebarSearchInput'
})
export class SidebarSearchInputPipe implements PipeTransform {

  transform(users: UserDetails[], searchInput:string): UserDetails[] {
    if (!searchInput) {
      return users;
    }
    const numericInput = parseInt(searchInput, 10);
    const isNumeric = !isNaN(numericInput);
    return users.filter((user: UserDetails) => {
      return user.username.toLowerCase().includes(searchInput.toLowerCase()) ||
        (isNumeric && user.age === numericInput) ||
        (user.userType.toLowerCase().includes(searchInput.toLowerCase())) ||
        // user.password.toLowerCase().includes(searchInput.toLowerCase()) ||
        user.gender.toLowerCase().includes(searchInput.toLowerCase());
    });
  }

}
