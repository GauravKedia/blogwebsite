import { Component, OnInit } from '@angular/core';
import { UserDetails } from '../userDetails';
import { ActivatedRoute, Router } from '@angular/router';
import { BlogActionsService } from 'src/app/services/blog-actions.service';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.scss']
})
export class ManageUsersComponent implements OnInit {
  baseProfilePath: string = "assets/user_images/";
  users: UserDetails[] = [];
  profile_count: number = this.users.length;
  user_data: UserDetails = {
    username: "",
    age: 0,
    gender: "",
    password: "",
    profilePhoto: "assets/user_images/default_profile.png",
    userType: "User",
    dateOfBirth: new Date(),
    contact: 1234567890,
    countryCode: 91,
    height: 0
  };

  isNewUser: boolean = true;
  isEditMode: boolean = true;
  selectedUsername: string = "";
  dataError: { [key: string]: boolean } = {
    username: false,
    gender: false,
    age: false,
    password: false,
    profilePhoto: false,
    dateOfBirth: false,
    contact: false,
    countryCode: false,
    height: false
  };

  constructor(private route: ActivatedRoute, private router: Router, private blogService: BlogActionsService) { }

  ngOnInit(): void {
    if(this.blogService.validateForPageAccess()){
      const storedUsers = localStorage.getItem('users');
      if (storedUsers) {
        this.users = JSON.parse(storedUsers);
        this.profile_count = this.users.length;
      }
    }
    else{
      alert("Unauthorized Access, Please Login Again");
      this.blogService.signOut();
    }
  }

  saveUsersToLocalStorage(): void {
    localStorage.setItem('users', JSON.stringify(this.users));
    this.profile_count = this.users.length;
  }

  make_error_default(): void {
    this.dataError = {
      username: false,
      gender: false,
      age: false,
      password: false,
      profilePhoto: false,
      dateOfBirth: false,
      contact: false,
      countryCode: false,
      height: false
    };
  }

  make_user_data_default(): void {
    this.user_data = {
      username: "",
      age: 0,
      gender: "",
      password: "",
      profilePhoto: "assets/user_images/default_profile.png",
      userType: "User",
      dateOfBirth: new Date(),
      contact: 1234567890,
      countryCode: 91,
      height: 0
    };
    this.isNewUser = true;
    this.isEditMode = true;
  }

  handleAddUserClick(): void {
    this.make_user_data_default();
    this.make_error_default();
  }

  userGotSelected(username: string): void {
    const selectedUser = this.users.find(user => user.username === username);
    if (selectedUser) {
      this.user_data = { ...selectedUser };
      this.isNewUser = false;
      this.isEditMode = false;
      this.selectedUsername = this.user_data.username;
      this.make_error_default();
      console.log(this.user_data);
    }
  }

  FormOptionClicked(optionAction: string): void {
    switch (optionAction) {
      case 'Delete':
        this.deleteClick();
        break;
      case 'Save':
        this.saveClick();
        break;
      case 'Edit':
        this.editClick();
        break;
      default:
        alert("No action Taken");
        break;
    }
  }

  checkIfAnyError(): boolean {
    for (const key in this.dataError) {
      if (this.dataError[key]) {
        return true;
      }
    }
    return false;
  }

  saveClick(): void {
    this.make_error_default();
    this.checkUserDataValid();
    if (!this.checkIfAnyError()) {
      if (this.isNewUser && this.isEditMode) {
        this.users.push({ ...this.user_data });
        this.saveUsersToLocalStorage();
        this.make_user_data_default();
        this.selectedUsername = this.user_data.username;
      } else if (!this.isNewUser && this.isEditMode) {
        this.saveAfterEdit();
      }
    }
  }

  deleteClick(): void {
    this.make_error_default();
    if (this.user_data) {
      const index = this.users.findIndex(user => user.username === this.user_data.username);
      if (index >= 0) {
        this.users.splice(index, 1);
        this.saveUsersToLocalStorage();
        this.make_user_data_default();
      } else {
        alert("Cannot Delete User");
      }
    }
  }

  editClick(): void {
    this.isNewUser = false;
    this.isEditMode = true;
    this.make_error_default();
  }

  saveAfterEdit(): void {
    if (this.user_data) {
      const index = this.users.findIndex(user => user.username === this.selectedUsername);
      if (index >= 0) {
        this.users[index] = { ...this.user_data };
        this.saveUsersToLocalStorage();
        this.make_user_data_default();
        this.selectedUsername = this.user_data.username;
      } else {
        alert("Cannot Find User To Edit");
      }
    }
  }

  checkUserDataValid(): void {
    const trimmedUsername = this.user_data.username.trim();
    let isUsernameUnique = this.users.every(user => user.username !== trimmedUsername);
    // console.log(this.user_data);
    if (!isUsernameUnique && trimmedUsername === this.selectedUsername) {
      isUsernameUnique = true;
    }
    if (trimmedUsername.length == 0 || !isUsernameUnique) {
      this.dataError['username'] = true;
    } else {
      this.dataError['username'] = false;
    }

    const isGenderValid = this.user_data.gender === 'Male' || this.user_data.gender === 'Female';
    if (!isGenderValid) {
      this.dataError['gender'] = true;
    } else {
      this.dataError['gender'] = false;
    }

    const ispasswordValid = this.user_data.password ? this.user_data.password.trim().length > 5 : false;
    if (!ispasswordValid) {
      this.dataError['password'] = true;
    } else {
      this.dataError['password'] = false;
    }

    const isProfilePhotoValid = this.user_data.profilePhoto ? this.user_data.profilePhoto.trim().length > 0 : false;
    if (!isProfilePhotoValid) {
      this.dataError['profilePhoto'] = true;
    } else {
      this.dataError['profilePhoto'] = false;
    }

    function isValidDate(date: Date): boolean {
      return date instanceof Date && !isNaN(date.getTime());
    }

    function isDateBeforeToday(date: Date): boolean {
      const today = new Date();
      today.setHours(0, 0, 0, 0);
      return date < today;
    }


    const isDateOfBirthValid = isValidDate(this.user_data.dateOfBirth) && isDateBeforeToday(this.user_data.dateOfBirth);
    // console.log(isDateOfBirthValid);
    // console.log(this.user_data.dateOfBirth);
    // console.log(isValidDate(this.user_data.dateOfBirth));
    // console.log(isDateBeforeToday(this.user_data.dateOfBirth));
    if (!isDateOfBirthValid) {
      this.dataError['dateOfBirth'] = true;
    } else {
      this.dataError['dateOfBirth'] = false;
    }

    const isContactValid = /^\d{10}$/.test(this.user_data.contact.toString());
    if (!isContactValid) {
      this.dataError['contact'] = true;
    } else {
      this.dataError['contact'] = false;
    }

    const isCountryCodeValid = /^\d+$/.test(this.user_data.countryCode.toString());
    if (!isCountryCodeValid) {
      this.dataError['countryCode'] = true;
    } else {
      this.dataError['countryCode'] = false;
    }

    const isHeightValid = this.user_data.height > 0;
    // console.log(isHeightValid);
    // console.log(this.user_data.height);
    if (!isHeightValid) {
      this.dataError['height'] = true;
    } else {
      this.dataError['height'] = false;
    }

  }

}
