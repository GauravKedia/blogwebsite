import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import { UserDetails } from '../../userDetails';

@Component({
  selector: 'app-manage-users-form',
  templateUrl: './manage-users-form.component.html',
  styleUrls: ['./manage-users-form.component.scss']
})
export class ManageUsersFormComponent implements OnInit {

  @Input() user_data: UserDetails = {
    username: "",
    age: 0,
    gender: "",
    password: "",
    profilePhoto: "assets/user_images/default_profile.png",
    userType: "User",
    dateOfBirth: new Date(),
    contact: 0,
    countryCode: 91,
    height: 0
  };
  @Input() isEditMode: boolean = true;
  @Input() baseProfilePath = "assets/user_images/";
  @Output() FormOptionClicked = new EventEmitter<string>();
  @Input() dataError: { [key: string]: Boolean } = {
    username: false,
    gender: false,
    age: false,
    password: false,
    profilePhoto: false,
    dateOfBirth: false,
    contact: false,
    countryCode: false,
    height: false
  };

  // viewModeCommonViews:string[] = ['Username','Gender','password','Gender'];
  OptionClicked(optionAction: string) {
    this.FormOptionClicked.emit(optionAction);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['dataError'] && changes['dataError'].currentValue) {
      this.dataError = changes['dataError'].currentValue;
    }
    // console.log(changes);
  }


  onFileChanged(event: Event) {
    const inputFile = event.target as HTMLInputElement;
    if (inputFile.files && inputFile.files.length > 0) {
      const img = inputFile.files[0];
      if (img) {
        const reader = new FileReader();
        reader.readAsDataURL(img);
        reader.onload = () => {
          if (typeof reader.result === 'string') {
            this.user_data.profilePhoto = reader.result;
          }
        }
      }
    }
  }

  onDateOfBirthChange(dateString: string) {
    this.user_data.dateOfBirth = new Date(dateString);
    const dob = new Date(this.user_data.dateOfBirth);
    const today = new Date();
    const age = today.getFullYear() - dob.getFullYear();
    const monthDiff = today.getMonth() - dob.getMonth();
    if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < dob.getDate())) {
      this.user_data.age = age - 1;
    }
    this.user_data.age = age;
  }
  constructor() { }

  ngOnInit(): void {

  }
}
