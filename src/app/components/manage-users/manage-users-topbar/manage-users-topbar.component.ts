import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-manage-users-topbar',
  templateUrl: './manage-users-topbar.component.html',
  styleUrls: ['./manage-users-topbar.component.scss']
})
export class ManageUsersTopbarComponent implements OnInit {
  @Output() addUserClick = new EventEmitter<void>();
  addUser(){
    this.addUserClick.emit();
  }
  constructor() { }

  ngOnInit(): void {
  }
 

}
