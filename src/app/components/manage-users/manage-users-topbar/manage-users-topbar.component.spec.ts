import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageUsersTopbarComponent } from './manage-users-topbar.component';

describe('ManageUsersTopbarComponent', () => {
  let component: ManageUsersTopbarComponent;
  let fixture: ComponentFixture<ManageUsersTopbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageUsersTopbarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ManageUsersTopbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
