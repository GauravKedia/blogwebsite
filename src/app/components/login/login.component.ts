import { JsonPipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder,FormGroup, Validators } from '@angular/forms';
import { UserDetails } from '../userDetails';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  userDetails: UserDetails[] = [{
    username: 'admin',
    age: 20,
    gender: 'Male',
    password: 'admin',
    profilePhoto: 'assets/user_images/default_profile.png',
    userType: 'Admin',
    dateOfBirth: new Date('2002-01-01'),
    contact: 123456789,
    countryCode: 91,
    height: 20,
  }];
  loginForm: FormGroup = this.formBuilder.group({
    username: ['',Validators.required],
    password: ['',Validators.required]
  });
  isAdmin: boolean = false;
  loggedInUser: string = '';

  constructor(private route: ActivatedRoute, private router: Router,private formBuilder: FormBuilder  ) { }

  ngOnInit() {
    const storedUsers: UserDetails[] = JSON.parse(localStorage.getItem('users') || '[]');
    // console.log(storedUsers);
    if(storedUsers.length===0){
      localStorage.setItem('users', JSON.stringify(this.userDetails));
    }
    // localStorage.setItem('usersLogin',JSON.stringify({username: this.adminDetail.password, password: this.adminDetail.password }));
  }

  login(event: Event) {
    if(this.loginForm.valid){
      const storedUsers: UserDetails[] = JSON.parse(localStorage.getItem('users') || '[]');
      const user = storedUsers.find(user => user.username === this.loginForm.value.username);
      // console.log(storedUsers);
      // console.log(user);
      // console.log(this.loginDetail.username);
      // console.log(this.loginDetail.password);
      if (user) {
        if (user.password === this.loginForm.value.password) {
          this.loggedInUser = user.username;
          localStorage.setItem('loggedInUser', this.loggedInUser);
          if (user.userType === 'Admin') {
            this.isAdmin = true;
            localStorage.setItem('isAdmin', 'true');
          } else {
            this.isAdmin = false;
            localStorage.setItem('isAdmin', 'false');
          }
          this.router.navigate(['/home']);
        } else {
          alert('Invalid Password');
        }
      } else {
        alert('User not found');
      }
    }
  }
}
