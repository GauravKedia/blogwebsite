import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './components/home/home.component';
import { HomeBlogComponent } from './components/home/home-blog/home-blog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { HomeSidebarComponent } from './components/home/home-sidebar/home-sidebar.component';
import { ManageUsersComponent } from './components/manage-users/manage-users.component';
import { ManageUsersSidebarComponent } from './components/manage-users/manage-users-sidebar/manage-users-sidebar.component';
import { SidebarSearchInputPipe } from './components/manage-users/manage-users-sidebar/sidebar-search-input.pipe';
import { UserComponent } from './components/manage-users/manage-users-sidebar/user/user.component';
import { ManageUsersTopbarComponent } from './components/manage-users/manage-users-topbar/manage-users-topbar.component';
import { ManageUsersFormComponent } from './components/manage-users/manage-users-form/manage-users-form.component';
import { ManageUserFormDataPipe } from './components/manage-users/manage-users-form/manage-user-form-data.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { HoverDirective } from './components/home/home-blog/hover.directive';
import { DisplayBlogComponent } from './components/display-blog/display-blog.component';
import { ReduceContentPipe } from './components/home/home-blog/reduce-content.pipe';
import { BlogTemplateComponent } from './components/common/blog-template/blog-template.component';
import { PrivateBlogsComponent } from './components/private-blogs/private-blogs.component';
import { PublicBlogsComponent } from './components/public-blogs/public-blogs.component';
import { CreateBlogComponent } from './components/create-blog/create-blog.component';
import { HoverBlogDirective } from './components/common/blog-template/hover-blog.directive';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    HomeBlogComponent,
    CreateBlogComponent,
    HomeSidebarComponent,
    ManageUsersComponent,
    ManageUsersSidebarComponent,
    SidebarSearchInputPipe,
    UserComponent,
    ManageUsersTopbarComponent,
    ManageUsersFormComponent,
    ManageUserFormDataPipe,
    HoverDirective,
    DisplayBlogComponent,
    ReduceContentPipe,
    BlogTemplateComponent,
    PrivateBlogsComponent,
    PublicBlogsComponent,
    HoverBlogDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    MatIconModule,
    FormsModule,
    NoopAnimationsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
