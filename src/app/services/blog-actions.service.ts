import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BlogPost } from '../components/home/blog_interface';

@Injectable({
  providedIn: 'root'
})
export class BlogActionsService {

  private createEmptyBlogPost(blogId: string): BlogPost {
    return {
      id: blogId,
      creator: '',
      title: '',
      description: '',
      isPrivate: false,
      content: ''
    };
  }
  
  
  isLoggedIn(): boolean {
    const loggedInUser = localStorage.getItem('loggedInUser');
    return !!loggedInUser && loggedInUser.trim() !== '';
  }

  validateForPageAccess(): boolean{
    const checkAdminPresent = localStorage.getItem('isAdmin');
    return this.isLoggedIn() && !!checkAdminPresent;
  }

  getLoggedInUsername(): string | null {
    if (this.isLoggedIn()) {
      return localStorage.getItem('loggedInUser');
    } else {
      return null; 
    }
  }
  
  loadPublicBlogs(): BlogPost[] {
      const storedBlogPosts: BlogPost[] = JSON.parse(localStorage.getItem('blogPosts') || '[]');
      return storedBlogPosts.filter(blog => !blog.isPrivate);
  }

  loadPrivateBlogs(): BlogPost[] {
    const creator = this.getLoggedInUsername();
    const storedBlogPosts: BlogPost[] = JSON.parse(localStorage.getItem('blogPosts') || '[]');
    return storedBlogPosts.filter(blog => blog.creator === creator);;
  }

  getBlogFromId(blogId: string): BlogPost | undefined {
    const storedBlogPosts: BlogPost[] = JSON.parse(localStorage.getItem('blogPosts') || '[]');
    const blog = storedBlogPosts.find(blog => blog.id === blogId);
    return blog;
  }

  signOut(): void {
    localStorage.removeItem('loggedInUser');
    localStorage.removeItem('isAdmin');
    this.router.navigate(['/login']);
  }

  isAdminUser(): boolean {
    const isAdminString = localStorage.getItem('isAdmin');
    return isAdminString === 'true';
  }

  addBlog(blog: BlogPost): void {
    let storedBlogPosts: BlogPost[] = JSON.parse(localStorage.getItem('blogPosts') || '[]');
    storedBlogPosts.push(blog);
    localStorage.setItem('blogPosts', JSON.stringify(storedBlogPosts));
    this.router.navigate(['/home']);
  } 


  editBlog(blog: BlogPost): boolean {
    let storedBlogPosts: BlogPost[] = JSON.parse(localStorage.getItem('blogPosts') || '[]');
    const index = storedBlogPosts.findIndex(b => b.id === blog.id);
    if (index !== -1) {
      storedBlogPosts[index] = blog;
      localStorage.setItem('blogPosts', JSON.stringify(storedBlogPosts));
      this.router.navigate(['/home']);
      return true;
    }
    return false;
  }


  validateBlog(blog: BlogPost): boolean {
    return !!blog.title && !!blog.description;
  }
  

  deleteBlog(blogId: string): boolean {
    const postData: BlogPost[] = JSON.parse(localStorage.getItem('blogPosts') || '[]');
    const index = postData.findIndex(blog => blog.id === blogId);
    if (index !== -1) {
      postData.splice(index, 1);
      localStorage.setItem('blogPosts', JSON.stringify(postData));
      this.router.navigate(['/home']);
      return true;
    }
    return false;
  }
  
  
  
  constructor(private router: Router) { }
}
