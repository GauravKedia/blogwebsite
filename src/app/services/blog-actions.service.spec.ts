import { TestBed } from '@angular/core/testing';

import { BlogActionsService } from './blog-actions.service';

describe('BlogActionsService', () => {
  let service: BlogActionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BlogActionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
