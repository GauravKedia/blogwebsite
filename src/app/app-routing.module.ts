import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { ManageUsersComponent } from './components/manage-users/manage-users.component';
import { PublicBlogsComponent } from './components/public-blogs/public-blogs.component';
import { PrivateBlogsComponent } from './components/private-blogs/private-blogs.component';
import { BlogTemplateComponent } from './components/common/blog-template/blog-template.component';
import { DisplayBlogComponent } from './components/display-blog/display-blog.component';
import { CreateBlogComponent } from './components/create-blog/create-blog.component';
 const routes: Routes = [
   { path: 'login', component: LoginComponent },
   { 
     path: 'home', 
     component: HomeComponent,
     children: [
       { path: 'view-blog/:blogId', component: DisplayBlogComponent },
       { path: 'my-blogs', component: PrivateBlogsComponent },
       { path: 'create-blog/:blogId', component:CreateBlogComponent},
       { path: '**', component: PublicBlogsComponent },
      ]
    },
    { path: 'manage', component: ManageUsersComponent },
    { path: '**' , component: LoginComponent},
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
